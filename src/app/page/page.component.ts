import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import * as jexcel from 'jexcel';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  @ViewChild('tableItem', {static: true}) tableItem: ElementRef;
  
  myForm: FormGroup;
  private excelTable: any;
  chartBarStacked = false;

  data = [
    {
      labels: ['', '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Oct']
    },
    {
      data: ['#5054a4', 'Series 1', 7, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3]
    },
    {
      data: ['#a8be60', 'Series 2', 2, 0.8, 5.7, 11.3, 17, 22, 24.8, 24.1, 20.1]
    },
    {
      data: ['#c96120', 'Series 3', 9, 0.6, 3.5, 8.4, 13.5, 17, 18.6, 17.9, 14.3]
    },
    {
      data: ['#7d6f71', 'Series 4', 3.9, 4.2, 6.7, 8.5, 11.9, 15.2, 17, 16.6, 14.2]
    }
  ]

  chartType = 'line';

  chartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        },
      }],
      yAxes: [{
        stacked: true,
        gridLines: {
          zeroLineColor: 'rgba(0,0,0,0)',
          display: true
        },
        ticks: {
          autoSkip: true,
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
  };

  dataSets = [];
  chartDatasets = [];
  labels = [];
  chartLabels = [];
  chartColors = [];

  constructor() { }

  ngOnInit(): void {
    this.myForm = new FormGroup({
      charttype: new FormControl('line')
    });
    
    this.myForm.get('charttype').valueChanges.subscribe(value => {
      if (value === 'stacked') {
        this.chartType = 'bar'
        this.chartBarStacked = true;
      } else {
        this.chartType = value;
        this.chartBarStacked = false;
      }
   });

   this.createData(this.data);
   this.createChartData(this.labels, this.dataSets);
   this.createTableData(this.labels, this.dataSets);
  }
  
  createData(data) {
    this.labels = [];
    this.dataSets = [];
    data.forEach(el => {
      if (el.labels) {
        this.labels = el.labels;
      } else {
        this.dataSets.push(el);
      }
    });    
  }

  createChartData(labels, datasets) {
    this.chartLabels = [];
    this.chartDatasets = [];
    this.chartColors = [];

    console.log('changing')
    this.chartLabels = labels.slice(2);

    datasets.forEach(el => {
      this.chartDatasets.push({label: el.data[1], data: el.data.slice(2), fill: false});
      this.chartColors.push({backgroundColor: this.convertHexToRgba(el.data[0], 0.8), borderColor: this.convertHexToRgba(el.data[0], 1), borderWidth: 1});
    });
  }

  convertHexToRgba(hex: string, opacity: number) {
    let col: any;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        col = hex.substring(1).split('');
        if(col.length === 3){
            col = [col[0], col[0], col[1], col[1], col[2], col[2]];
        }
        col= '0x' + col.join('');
        return 'rgba(' + [(col >> 16) & 255, (col >> 8) & 255, col & 255].join(',') + ',' + opacity + ')';
    }
    throw new Error('Bad Hex');
  }

  createTableData(labels, datasets) {
    let tableDataSets = [];
    
    tableDataSets.push(labels);

    datasets.forEach(el => {
      tableDataSets.push(el.data)
    });

    this.excelTable = jexcel(this.tableItem.nativeElement, {
      columnSorting:false,
      data: tableDataSets,
      columns: [
        {type: 'color', render:'square'},
      ],
      onchange: (instance, cell, x, y, value) => {
        this.createChartData(this.labels, this.dataSets);
      },
      oninsertrow: (instance) => {
      },
      oninsertcolumn: (instance) => {
      },
      ondeleterow: (value) => {
      }
    });
  }

  insertRow() {
    let newArr = [...this.data].slice(-1);
    this.data.push(...newArr as any);
    console.log('NEW DATA AFTER PUSH --- ', this.data)
    this.excelTable.insertRow(['#7d6f71', 'Series 5', 3.9, 4.2, 6.7, 8.5, 11.9, 15.2, 17, 16.6, 14.2]);
    this.createData(this.data);
    this.createChartData(this.labels, this.dataSets);
  }

}
